angular.module('app.controllers', [])
  
.controller('dashboardCtrl', function($scope,$ionicModal,TestFactory,DefectFactory,UserFactory) {
    //$scope.requirements = requirements;
    $scope.testCases = testCases;
    $scope.defects = defects;
    $scope.numOfDefects = $scope.defects.length;
    $scope.numOfTestsPassed = 0;
    $scope.numOfTestsTotal = $scope.testCases.length;
    $scope.shouldShowDelete = true;
    $scope.listCanSwipe = true;
   
    for(i=0; i < $scope.testCases.length; i++){
        if($scope.testCases[i].isPassed === true ){
            $scope.numOfTestsPassed += 1 ;
        }
    }
    
    $scope.projectProgress = ($scope.numOfTestsPassed / $scope.numOfTestsTotal ) * 100;
    
    $scope.users = users;
    
    $ionicModal.fromTemplateUrl('templates/addTeamMember.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });
    
})
   
.controller('requirementsCtrl', function($scope,$ionicModal,RequirementFactory) {
  
  /*$http.get("http://localhost:3000/api/Projects/575c3c487d6775adc9dcfec1/requirements?access_token=19iMlRJ5rImZbZfXFsnRaY31dUyJpaCyYysAwKcAij8my62DrZLzkPv4b1JUfuFA").then(function(response){
       $scope.requirements = response.data;
     }       
    ); */ 
  RequirementFactory.query(
        function (response) {
            $scope.requirements = response.data;
        },
        function (response) {
        });
        
  //$scope.requirements = requirements;
    
  $ionicModal.fromTemplateUrl('templates/addRequirements.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });
  
  $scope.createRequirement = function(u) {        
    $scope.requirements.push({ name: u.name + ' ' + u.description });
    $scope.modal.hide();
  };
})

.controller('requirementsDetailCtrl', function($scope,$stateParams,RequirementFactory) {
    $scope.requirement = requirements[$stateParams.id];
})
   
.controller('testingCtrl', function($scope,$ionicModal,$ionicPopup,TestFactory) {
    $scope.testCases = testCases;
    $scope.listCanSwipe = true;
    $scope.shouldShowFailed = false;
    
    $ionicModal.fromTemplateUrl('templates/addTestCase.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });
     
    $scope.passed = function(index) {
        var alertPopup = $ionicPopup.alert({
            title: 'Test Result!',
            template: index.name + " passed"
        });

        return alertPopup.then(function(res) {
            console.log('Passing ' + index.name);
        });
    };
    
    $scope.failed = function(index) {
        var alertPopup = $ionicPopup.alert({
            title: 'Test Result!',
            template: index.name + " failed"
        });

        return alertPopup.then(function(res) {
            console.log('Failing ' + index.name);
        });
    };
})

.controller('testCaseCtrl', function($scope,$stateParams,$ionicPopup,TestFactory) {
    //$scope.id = $stateParams.id;
    $scope.testCase = testCases[$stateParams.id];
    
    $scope.passed = function(index) {
        var alertPopup = $ionicPopup.alert({
            title: 'Test Result!',
            template: index.name + " passed"
        });

        return alertPopup.then(function(res) {
            console.log('Passing ' + index.name);
        });
    };
    
    $scope.failed = function(index) {
        var alertPopup = $ionicPopup.alert({
            title: 'Test Result!',
            template: index.name + " failed"
        });

        return alertPopup.then(function(res) {
            console.log('Failing ' + index.name);
        });
    };
    
    $scope.remove = function() {
        var alertPopup = $ionicPopup.alert({
            title: 'Remove Attachment!',
            template: " removed"
        });

        return alertPopup.then(function(res) {
            console.log('Removing ');
        });
    };
})

.controller('defectsCtrl', function($scope,$ionicModal,DefectFactory) {
    $scope.defects = defects;
    
    $ionicModal.fromTemplateUrl('templates/addDefect.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });
})

.controller('defectsDetailCtrl', function($scope,$stateParams,DefectFactory) {
    $scope.defect = defects[$stateParams.id];
})

.controller('reportsCtrl', function($scope,RequirementFactory,TestFactory) {
    $scope.requirements = requirements;
    $scope.testCases = testCases;
})
      
.controller('loginCtrl', function($scope) {

})
   
.controller('signupCtrl', function($scope) {

})
   
.controller('addTestCaseCtrl', function($scope) {

})
   
.controller('addDefectCtrl', function($scope) {

})
   
.controller('editPrintParametersCtrl', function($scope) {

})
   
.controller('exportReportCtrl', function($scope) {

})
   
.controller('addCommentCtrl', function($scope) {

})
   
.controller('addAttachmentCtrl', function($scope) {

})
   
.controller('addTeamMemberCtrl', function($scope) {

})
   
.controller('notifyAllCtrl', function($scope) {

})
   
.controller('profileSettingsCtrl', function($scope) {

})
   
.controller('aboutCtrl', function($scope) {

})
   
.controller('contactCtrl', function($scope) {

})


  