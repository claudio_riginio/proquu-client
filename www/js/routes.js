angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
  

  .state('menu.dashboard', {
    url: '/dashboard',
    views: {
      'side-menu21': {
        templateUrl: 'templates/dashboard.html',
        controller: 'dashboardCtrl'
      }
    }
  })

  .state('menu.requirements', {
    url: '/requirements',
    views: {
      'side-menu21': {
        templateUrl: 'templates/requirements.html',
        controller: 'requirementsCtrl'
      }
    }
  })

  .state('menu.testing', {
    url: '/testing',
    views: {
      'side-menu21': {
        templateUrl: 'templates/testing.html',
        controller: 'testingCtrl'
      }
    }
  })

  .state('menu.defects', {
    url: '/defects',
    views: {
      'side-menu21': {
        templateUrl: 'templates/defects.html',
        controller: 'defectsCtrl'
      }
    }
  })

  .state('menu.reports', {
    url: '/reports',
    views: {
      'side-menu21': {
        templateUrl: 'templates/reports.html',
        controller: 'reportsCtrl'
      }
    }
  })

  .state('menu', {
    url: '/side-menu21',
    templateUrl: 'templates/menu.html',
    abstract:true
  })

  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl'
  })

  .state('signup', {
    url: '/signup',
    templateUrl: 'templates/signup.html',
    controller: 'signupCtrl'
  })

  .state('menu.testCase', {
    url: '/testing/:id',
    views: {
      'side-menu21': {
        templateUrl: 'templates/testCase.html',
        controller: 'testCaseCtrl'
      }
    }
  })

  .state('menu.requirementsDetail', {
    url: '/requirements/:id',
    views: {
      'side-menu21': {
        templateUrl: 'templates/requirementsDetail.html',
        controller: 'requirementsDetailCtrl'
      }
    }
  })

  .state('menu.defectsDetail', {
    url: '/defects/:id',
    views: {
      'side-menu21': {
        templateUrl: 'templates/defectsDetail.html',
        controller: 'defectsDetailCtrl'
      }
    }
  })

  .state('editPrintParameters', {
    url: '/editprint',
    templateUrl: 'templates/editPrintParameters.html',
    controller: 'editPrintParametersCtrl'
  })

  .state('exportReport', {
    url: '/exportreport',
    templateUrl: 'templates/exportReport.html',
    controller: 'exportReportCtrl'
  })

  .state('addComment', {
    url: '/addcomment',
    templateUrl: 'templates/addComment.html',
    controller: 'addCommentCtrl'
  })

  .state('addAttachment', {
    url: '/addattachment',
    templateUrl: 'templates/addAttachment.html',
    controller: 'addAttachmentCtrl'
  })

  .state('notifyAll', {
    url: '/notifyall',
    templateUrl: 'templates/notifyAll.html',
    controller: 'notifyAllCtrl'
  })

  .state('menu.profileSettings', {
    url: '/profile',
    views: {
      'side-menu21': {
        templateUrl: 'templates/profileSettings.html',
        controller: 'profileSettingsCtrl'
      }
    }
  })

  .state('menu.about', {
    url: '/about',
    views: {
      'side-menu21': {
        templateUrl: 'templates/about.html',
        controller: 'aboutCtrl'
      }
    }
  })

  .state('menu.contact', {
    url: '/contact',
    views: {
      'side-menu21': {
        templateUrl: 'templates/contact.html',
        controller: 'contactCtrl'
      }
    }
  })

$urlRouterProvider.otherwise('/login')

  

});