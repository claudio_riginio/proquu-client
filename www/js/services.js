angular.module('app.services', ['ngResource'])
.constant("baseURL", "http://localhost:3000/")

.factory('RequirementFactory', ['$resource', function($resource){
       return $resource("http://localhost:3000/api/Projects/575c3c487d6775adc9dcfec1/requirements?access_token=19iMlRJ5rImZbZfXFsnRaY31dUyJpaCyYysAwKcAij8my62DrZLzkPv4b1JUfuFA");
   }]
)

.factory('TestFactory', [function(){
    return testCases = [{
        id: 0,
        name: "Test Case 1",
        description: "Switch on the device!",
        testResultTxt: "Everything is fine!",
        testResult: "failed",
        isPassed: false,
        isFailed: true,
        requirementRef: [
            {
            ref: "Requirmenet ID131"
            }
        ],
        attachment: [
            {
            ref: "http://www.br.de/fernsehen/ard-alpha/sendungen/schulfernsehen/berufswahl-elektronik-mechatroniker-100~_v-img__16__9__l_-1dc0e8f74459dd04c91a0d45af4972b9069f1135.jpg?version=641bf"
            }
        ],
        comments: [
            {
            comment: "Imagine all the eatables, living in conFusion!",
            author: "John Lemon",
            date: "2012-10-16T17:57:28.556094Z"
            },
            {
            comment: "living in conFusion!",
            author: "Me",
            date: "2012-10-16T17:57:28.556094Z"
            },
            {
            comment: "Imagination!",
            author: "Clur Mag",
            date: "2015-10-16T17:57:28.556094Z"
            }
        ],
        created: {
            by: "John Lemon",
            on: "2012-10-16"
        },
        changed: {
            by: "Paul Cartner",
            on: "2012-10-16"
        }
        },
        {
        id: 1,
        name: "Test Case 2",
        description: "Switch off the device!",
        testResultTxt: "Everything is fine!",
        testResult: "passed",
        isPassed: true,
        isFailed: false,
        requirementRef: [
            {
            ref: "Requirmenet ID131"
            }
        ],
        attachment: [
            {
            ref: "http://www.physik.uni-regensburg.de/studium/edverg/elfort/ElektronikBand.jpg"
            }
        ],
        comments: [
            {
            comment: "Imagine all the eatables, living in conFusion!",
            author: "John Lemon",
            date: "2012-10-16T17:57:28.556094Z"
            }
        ],
        created: {
            by: "John Lemon",
            on: "2012-10-16"
        },
        changed: {
            by: "Paul Cartner",
            on: "2012-10-16"
        }
        }]
    }]
)

.factory('DefectFactory', [function(){
    return defects = [{
        id: 0,
        name: "Defect 1",
        description: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.",
        attachment: [
            {
            ref: "http://www.br.de/fernsehen/ard-alpha/sendungen/schulfernsehen/berufswahl-elektronik-mechatroniker-100~_v-img__16__9__l_-1dc0e8f74459dd04c91a0d45af4972b9069f1135.jpg?version=641bf"
            }
        ],
        testCaseRef: [
            {
            ref: "TestCase ID31"
            }
        ],
        comments: [
            {
            comment: "Imagine all the eatables, living in conFusion!",
            author: "John Lemon",
            date: "2012-10-16T17:57:28.556094Z"
            },
            {
            comment: "living in conFusion!",
            author: "Me",
            date: "2012-10-16T17:57:28.556094Z"
            },
            {
            comment: "Imagination!",
            author: "Clur Mag",
            date: "2015-10-16T17:57:28.556094Z"
            }
        ],
        created: {
            by: "John Lemon",
            on: "2012-10-16"
        },
        changed: {
            by: "Paul Cartner",
            on: "2012-10-16"
        }
        },
        {
        id: 1,
        name: "Defect 2",
        description: "Switch function! Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet",
        attachment: [
            {
            ref: "http://www.physik.uni-regensburg.de/studium/edverg/elfort/ElektronikBand.jpg"
            }
        ],
        testCaseRef: [
            {
            ref: "TestCase ID51"
            }
        ],
        comments: [
            {
            comment: "Imagine all the eatables, living in conFusion!",
            author: "John Lemon",
            date: "2012-10-16T17:57:28.556094Z"
            }
        ],
        created: {
            by: "John Lemon",
            on: "2012-10-16"
        },
        changed: {
            by: "Paul Cartner",
            on: "2012-10-16"
        }
        }]
    }]
)

.factory('BlankFactory', [function(){
   
}])

.factory('UserFactory', [function(){
   return users = [{
        id: 0,
        firstName: "Sterling",
        lastName: "Archer",
        created: {
            on: "2012-10-16"
        },
        foto: "img/l9ufLGmSQuoa7rBFEuLP_Archer.jpg"
   },
   {
        id: 1,
        firstName: "Cyril",
        lastName: "Figgis",
        created: {
            on: "2012-10-16"
        },
        foto: "img/kLVVbSFR5WCXost8g3Lm_Cyril_Figgis.jpg"
   },
   {
        id: 2,
        firstName: "Lana",
        lastName: "Kane",
        created: {
            on: "2012-10-16"
        },
        foto: "img/M2CANngrQCGR8Ygk8eyG_lana_kane.jpg"
   }]
}])

.factory('DashboardFactory', [function($http,baseURL){
    return defects = {
        No: 18,name: "switch", 
        category: "Devices"
    };
   
}])

.service('BlankService', ['$resource', 'baseURL',function($resource, baseURL){
    
}]);